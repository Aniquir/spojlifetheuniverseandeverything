import java.util.*;

/**
 * Created by Paweł on 2017-08-16.
 */
public class Main {

    public void print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }

        for (int x : array){
            if (x != 42){
                System.out.println(x);
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {

        Main x = new Main();
        int[] array = new int[50];
        x.print(array);

    }
}
// na stronie pokazuje runtime, program dziala, ale cos jest nie tak
//nadal nie dziala, teraz sie wyswietla, ze jest zla odpowiedz
//dziwne to, narazie to oposc, wrocisz jak zrobisz inne 
